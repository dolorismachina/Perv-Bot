# -*- coding: utf-8 -*-
from url import URL


# Single reddit post
class RedditSubmission(object):

    def __init__(self, url, title, author, date):
        self.url = URL.trim_album(url)
        self.title = title
        self.author = author
        self.date = date