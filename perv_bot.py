import json
import os
import sys
import urllib2
from bs4 import BeautifulSoup

from reddit_submission import RedditSubmission
from url import URL


# Get imgur urls from the reddit json. Returns a list of urls.
def get_submissions(json):
    urls = []
    for entry in json['data']['children']:
        url = entry['data']['url']
        title = entry['data']['title']
        author = entry['data']['author']
        date = entry['data']['created']

        sub = RedditSubmission(url, title, author, date)
        urls.append(sub)

    return urls


# This function divides the urls into lists of direct links, albums
# and other types of url
def sort_urls(submissions):
    direct_urls = []
    album_urls = []
    indirect_urls = []

    for submission in submissions:
        if URL.url_is_direct(submission.url):
            direct_urls.append(submission)

        elif URL.url_is_album(submission.url):
            print submission.url
            try:
                urllib2.urlopen(submission.url)
                submission.url += "/all/"
                album_urls.append(submission)
            except urllib2.HTTPError:
                print "404"
                continue

        elif URL.url_is_indirect(submission.url):
            indirect_urls.append(submission)

    all_urls = {
        "direct": direct_urls,
        "indirect": indirect_urls,
        "album": album_urls
    }

    return all_urls


# Gets direct images from album url of single reddit submission.
def get_images_from_album(submission):
	image_list = []
	images_container = BeautifulSoup(urllib2.urlopen(submission.url).read()).find(id="imagelist")

	images = images_container.find_all('img')

	for img in images:
	    img = img.get('src')

	    # Add http: before the url because imgur is being a dick with its urls
	    img = "http:" + img;
	    
	    trimmed = URL.trim_extension(img)

	    full_size = URL.convert_miniature_to_full_image(trimmed)
	    image_list.append(full_size)

	return image_list


# Gets direct links to all available images.
def get_all_images(submissions):
    # Direct images
    images = []
    for img in submissions['direct']:
        images.append(img.url)

    # Albums
    for album in submissions['album']:
        for img in get_images_from_album(album):
            images.append(img)

def get_reddit_json():
    try:
        return json.loads(urllib2.urlopen("http://reddit.com/r/gonewild/top/.json?sort=top&t=day&limit=100").read())

    except urllib2.HTTPError:
        print "Could not connect to database."
        print "Please, try running the program again."
        sys.exit()

reddit_json = get_reddit_json()
reddit_submissions = get_submissions(reddit_json)
sorted_submissions = sort_urls(reddit_submissions)

# Download images from albums
i = 0
if not os.path.exists("images/"):
    print "Creating directory to store images..."
    os.mkdir("images")

for submission in sorted_submissions['album']:
    directory = "images/" + submission.author + " - " + str(int(submission.date)) + "/"
    if not os.path.exists(directory):
        print "Downloading new album..."
        os.mkdir(directory)
        for url in get_images_from_album(submission):
            url_end = url[len(url) - 4:]
            f = open(directory + str(i) + url_end, "wb")
            f.write(urllib2.urlopen(url).read())
            f.close()
            i += 1
        i = 0

# Downloads images from direct urls
for submission in sorted_submissions['direct']:
    url_end = submission.url[len(submission.url) - 4:]
    if not os.path.exists("images/" + submission.author + " - " + str(int(submission.date)) + url_end):
        print "Downloading new image..."
        f = open("images/" + submission.author + " - " + str(int(submission.date)) + url_end, "wb")
        f.write(urllib2.urlopen(submission.url).read())
        f.close()

def get_next_page_link(json):
    return "http://reddit.com/r/gonewild/?&limit=100&after=" + json['data']['after']
